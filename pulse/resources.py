"""
This module contains Flask-rest-jsonAPI specific resources.
More info here: https://flask-rest-jsonapi.readthedocs.io/en/latest/logical_data_abstraction.html?highlight=resources
"""

import pulse.model

from pulse.schema import PulseSchema
import flask_rest_jsonapi

class PulseDetail(flask_rest_jsonapi.ResourceDetail):
    schema = PulseSchema
    data_layer = {'session': pulse.model.db.session,
                  'model': pulse.model.Pulse,
                  }


class PulseList(flask_rest_jsonapi.ResourceList):
    schema = PulseSchema
    data_layer = {'session': pulse.model.db.session,
                  'model': pulse.model.Pulse,
                  }