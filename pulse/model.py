"""
Contains the ORM objects representing the pulses.
"""


from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Pulse(db.Model):
    """
    Describes a `pulse` entry in the database.
    There is not much validation going on here, since we delegate that
    to the `schema` module.
    """
    __tablename__ = 'pulse'
    id= db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    # Add a unique constraint for the 'name' field.We don't want to allow two pulses
    # with the same name.
    name = db.Column(db.String, unique=True)
    type = db.Column(db.String)
    max_rabi_rate = db.Column(db.Integer)
    polar_angle = db.Column(db.Float)

