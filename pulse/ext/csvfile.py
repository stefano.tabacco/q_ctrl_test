"""
Contains CSV related extensions for this app.
We want to be able to download all the pulses in CSV format as well as
generating pulses from an uploaded CSV file.
"""

import csv
from io import StringIO

from flask_rest_jsonapi import JsonApiException

from pulse import model, schema
import logging

logger = logging.getLogger(__name__)

class CSVPulsePublisher:
    """
    This class is in charge of:

    * Reading the contents of a CSV file and persist the matching Pulse objects into the database.
    * Reading all the Pulses from the database and save out a CSV representation of them.
    """
    def __init__(self, app):
        self.app = app

    def pulses_to_CSV(self):
        """
        Will query all the :class:Pulse objects in the database and return
         a CSV string that represents them.
        :return: CSV representation of all the Pulses in the database.

        """
        with self.app.app_context():
            si = StringIO()
            fieldnames = ["Name", "Type", "Maximum Rabi Rate", "Polar Angle"]
            writer = csv.DictWriter(si, fieldnames=fieldnames)

            writer.writeheader()
            all_pulses = model.Pulse.query.all()
            for p in all_pulses:
                writer.writerow({'Name': p.name,
                                 'Type': p.type,
                                 "Maximum Rabi Rate": p.max_rabi_rate,
                                 "Polar Angle": p.polar_angle})
                logger.debug('Writing pulse named: {}'.format(p.name))

            return si.getvalue()

    def CSV_to_pulses(self, filename):
        """
        Given the path to a csv file, generates the ORM objects from its contents.
        :param string filename: path to a CSV file.
        :return: list of :class:`model.Pulse` objects.
        """
        out_pulses = list()
        assert filename.endswith('.csv')

        # now we save all the contents in the DB
        with open(filename) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=["Name", "Type", "Maximum Rabi Rate", "Polar Angle"])

            headers = next(reader)
            # Check validity of CSV header.
            if headers != {"Name": "Name",
                           "Type": "Type",
                           "Maximum Rabi Rate": "Maximum Rabi Rate",
                           "Polar Angle": "Polar Angle"}:
                raise JsonApiException("The CSV header is not in the correct format.")

            for i, line in enumerate(reader):
                data = {
                    "data": {
                        "type": "pulse",
                        "attributes": line
                    }
                }

                # be sure to validate the data against the schema
                result = schema.PulseSchema(many=False, strict=True).load(data)
                assert result.errors == {}
                validated_data = result.data
                # add the pulse to the database's session.
                out_pulses.append(model.Pulse(name=validated_data['name'],
                                              type=validated_data['type'],
                                              max_rabi_rate=validated_data['max_rabi_rate'],
                                              polar_angle=validated_data['polar_angle']))

            return out_pulses
