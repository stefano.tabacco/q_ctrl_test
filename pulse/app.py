from flask import Flask

from pulse.model import db
from pulse import view


def create_app(configClass='config.DevelopmentConfig'):
    """
    Creates the main flask app.
    :param string configClass: Can be used to switch between different configurations.
    :return: L{flask.Flask}
    """
    flask_app = Flask(__name__)
    flask_app.config.from_object(configClass)

    # initialise the database
    db.init_app(flask_app)

    view.api.init_app(flask_app)
    flask_app.register_blueprint(view.csv_blueprint)
    return flask_app
