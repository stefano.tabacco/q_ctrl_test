from marshmallow_jsonapi.flask import Schema
from marshmallow_jsonapi import fields
from marshmallow import validate


class PulseSchema(Schema):
    """
    A schema describing a pulse.
    All the fields' validation should happen here.
    """

    class Meta:
        type_ = 'pulse'
        self_view = 'api.pulse_detail'
        self_view_kwargs = {'id': '<id>'}
        self_view_many = 'api.pulse_list'

    id = fields.Integer(as_string=True, dump_only=True)
    name = fields.String(required=True, allow_none=False,
                         load_from="Name", dump_to='Name')
    type = fields.String(required=True, validate=validate.OneOf(("Primitive".lower(),
                                                                 "CORPSE".lower(),
                                                                 "Gaussian".lower(),
                                                                 "CinBB".lower(),
                                                                 "CinSK".lower())),
                         load_from="Type", dump_to='Type')
    max_rabi_rate = fields.Integer(required=True, validate=validate.Range(min=0, max=100),
                                   load_from="Maximum Rabi Rate",
                                   dump_to="Maximum Rabi Rate")
    polar_angle = fields.Float(required=True, validate=validate.Range(min=0.0, max=1.0),
                               load_from="Polar Angle",
                               dump_to="Polar Angle")

