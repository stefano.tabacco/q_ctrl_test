import logging
import os

import flask_rest_jsonapi
from flask import Blueprint, request, make_response, url_for, render_template
from flask_rest_jsonapi.exceptions import ObjectNotFound, JsonApiException
from werkzeug.utils import secure_filename

from pulse import model, resources
from pulse.ext.csvfile import CSVPulsePublisher

logger = logging.getLogger(__name__)

# Create the main API endpoints
api = flask_rest_jsonapi.Api(blueprint=Blueprint('api', __name__, url_prefix='/api'))
api.route(resources.PulseList, 'pulse_list', '/pulses')
api.route(resources.PulseDetail, 'pulse_detail', '/pulses/<int:id>')

# Create a blueprint for CSV import/export
csv_blueprint = Blueprint('csv', __name__, url_prefix='/csv')


@csv_blueprint.route('/')
def index():
    return render_template('csv.html')


@csv_blueprint.route('/download', methods=['GET'])
def download_all_pulses():
    """
    Collects all the pulses from the database and exports a csv file for download
    :return: :class:`flask.Response`
    """
    from flask import current_app as app

    if request.method == 'GET':
        output = make_response(CSVPulsePublisher(app).pulses_to_CSV())
        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"

        return output

    return None


@csv_blueprint.route('/upload', methods=['GET', 'POST'])
def upload_pulses_from_csv_file():
    """
    Will read the contents of a csv file, generate one pulse per line and
    persist them into the db.
    :return: str
    """
    from flask import current_app as app
    if request.method == 'POST':

        if 'file' not in request.files:
            raise ObjectNotFound('No file specified')
        file = request.files['file']
        if file.filename == '':
            raise ObjectNotFound('Empty Filename')

        if file:
            if not file.filename.endswith('.csv'):
                raise JsonApiException('The selected file is not of type .csv')

            filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file.filename))
            file.save(filename)
            logger.info('Saved CSV file at {!r}'.format(filename))

            pulses = CSVPulsePublisher(app).CSV_to_pulses(filename)
            model.db.session.bulk_save_objects(pulses)

            try:
                # commit all the pulses in one transaction.
                model.db.session.commit()
            except Exception as e:
                model.db.session.rollback()
                raise flask_rest_jsonapi.exceptions.BadRequest("Failed to save pulses to the database. \nReason:"
                                                               "{}".format(e))

            return 'Saved {} new pulses.You can view them at' \
                   ' <a href="{}">pulses</a> <br>.'.format(len(pulses),
                                                           url_for('api.pulse_list'))

    return '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form method=post enctype=multipart/form-data>
          <input type=file name=file>
          <input type=submit value=Upload>
        </form>
        '''
