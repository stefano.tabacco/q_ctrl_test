# Q-CTRL Challenge

This code was created for the back-end Q-CTRL challenge.

### Live Demo
There is a live instance of this demo running at: https://q-ctrl-test-stefano.herokuapp.com



### Local Installation

If you instead are interested in trying this locally with the development server you can do it by:

* Intall `pipenv` installed, in case you haven't already:
`pip install pipenv`

then from the root of the git repository

`pipenv install` to install all dependencies.

`pipenv install --dev` to install all dependencies and be able to run the tests.

#### Executing the application locally

To run the application locally in dev mode:

`pipenv run python run.py`

Check out the `curl_examples.sh` file for some usage example. Otherwise you can use PostMan or similar applications
for the usual CRUD operations.

#### Running the tests:
`pipenv run pytest`


