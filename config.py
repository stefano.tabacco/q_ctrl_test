"""
Some basic configuration for the project.
We'll be swapping them during different contexts.
"""

import os
import tempfile

class Config:
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = None
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PAGE_SIZE = 5  # default pagination set to 5
    UPLOAD_FOLDER = "static/dropbox"


class ProductionConfig(Config):
    """
    Points to a mysql database instance.
    """
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')

class DevelopmentConfig(Config):
    """
    During development, we will use an sqlite database.
    """
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///dev.db'
    MAX_CONTENT_LENGTH = 8 * 1024 * 1024


class TestingConfig(Config):
    """
    Used during unittesting.
    """
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    MAX_CONTENT_LENGTH = 2 * 1024 * 1024
    UPLOAD_FOLDER = tempfile.mkdtemp()
