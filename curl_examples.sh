echo ###################
echo Creating pulse named $name
echo ###################

name=$(uuidgen)

#url='https://q-ctrl-test-stefano.herokuapp.com'
url='http://localhost:5000'

echo Creating pulse named $name

curl -X POST \
  $url/api/pulses \
  -H 'accept: application/vnd.api+json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/vnd.api+json' \
  -d '{
	"data": {
		"type": "pulse",
		"attributes": {
			"Maximum Rabi Rate": 0,
			"Name": "cURL $name",
			"Polar Angle": 0.3,
			"Type": "primitive"
		}
	}
}
'

echo ###########################
echo Deleting Pulse with index 1
echo ############################

curl -X DELETE \
  $url/api/pulses/1 \
  -H 'accept: application/vnd.api+json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{}/127.0.0.1:5000
'

echo ###########################
echo Get All pulses (5 per page)
echo ###########################

curl -X GET \
  $url/api/pulses \
  -H 'accept: application/vnd.api+json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'


echo ###########################
echo Update A specific pulse
echo ###########################
curl -X PATCH \
  $url/api/pulses/22 \
  -H 'accept: application/vnd.api+json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
        "data": {
            "id": "22",
            "type": "pulse",
            "attributes": {
                "Maximum Rabi Rate": 100
            }

        }
    }'
    

echo #################################
echo Download all pulses in csv format
echo #################################
curl -X GET \
  $url/csv/download \
  -H 'accept: text/csv' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
  
  
echo #################################
echo Upload pulses from csv file
echo #################################
curl -F "file=@/tmp/testdata.csv" $url/csv/upload


