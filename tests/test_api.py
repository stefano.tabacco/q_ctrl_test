"""
Requirements for the api:

Manage pulses
Implement API endpoints that can:

Create a new pulse
List all pulses (five per page)
Get a specific pulse
Update a specific pulse
Delete a specific pulse

"""

HEADERS = {
    'accept': "application/vnd.api+json",
    'content-type': "application/vnd.api+json",
}


def test_can_list_all_pulses(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    response = flask_app.get('/api/pulses', headers=HEADERS)
    pulses = response.json
    assert pulses.get('meta').get('count') == 10
    assert len(pulses.get('data')) == setup_app_for_testing.config.get('PAGE_SIZE')


def test_can_list_pulses_with_pagination(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    response = flask_app.get('/api/pulses?page[size]=7', headers=HEADERS)
    pulses = response.json
    assert pulses.get('meta').get('count') == 10
    assert len(pulses.get('data')) == 7


def test_can_get_pulse(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    response = flask_app.get('/api/pulses/2', headers=HEADERS)
    assert response.json.get('data').get('attributes').get('Name') == 'Test pulse 1'


def test_can_create_pulse(setup_app_for_testing):
    data = {
        "data": {
            "type": "pulse",
            "attributes": {
                "Maximum Rabi Rate": 0,
                "Name": "test pulse posted 2",
                "Polar Angle": 0.3,
                "type": "primitive"
            }
        }
    }

    flask_app = setup_app_for_testing.test_client()
    response = flask_app.post('/api/pulses', json=data, headers=HEADERS)
    assert response.json
    assert "test pulse posted 2" == response.json.get('data').get('attributes').get('Name')
    assert '11' == response.json.get('data').get('id')


def test_can_update_pulse(setup_app_for_testing):
    data = {
        "data": {
            "id": "1",
            "type": "pulse",
            "attributes": {
                "Maximum Rabi Rate": 100,
            }

        }
    }

    flask_app = setup_app_for_testing.test_client()
    response = flask_app.get('/api/pulses/1', json=data, headers=HEADERS)
    assert response.json.get('data').get('attributes').get('Maximum Rabi Rate') == 0
    flask_app.patch('/api/pulses/1', json=data, headers=HEADERS)
    response = flask_app.get('/api/pulses/1', json=data, headers=HEADERS)
    assert response.json.get('data').get('attributes').get('Maximum Rabi Rate') == 100


def test_can_delete_pulse(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    response = flask_app.delete('/api/pulses/1', headers=HEADERS)
    assert '200 OK' == response.status

    # now deleting a non-existent pulse
    response = flask_app.delete('/api/pulses/2000', headers=HEADERS)
    assert '404' == response.status


def test_can_filter_pulses(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    query_primitive_pulses = 'api/pulses?filter=[{"name":"max_rabi_rate","op":"eq","val":"30"}]'
    response = flask_app.get(query_primitive_pulses, headers=HEADERS)
    assert '200 OK' == response.status
    assert 1 == len(response.json.get('data'))
    assert 30 == response.json.get('data')[0].get('attributes').get('Maximum Rabi Rate')

    # now querying a non-existing pulse
    query_primitive_pulses = 'api/pulses?filter=[{"name":"max_rabi_rate","op":"eq","val":"1000"}]'
    response = flask_app.get(query_primitive_pulses, headers=HEADERS)
    assert 0 == len(response.json.get('data'))
