import pytest

from pulse.app import create_app
from pulse.model import db, Pulse


@pytest.fixture()
def setup_app_for_testing():
    # swap the config for the test one
    flask_app = create_app('config.TestingConfig')
    setup_database_for_testing(flask_app)
    yield flask_app


def setup_database_for_testing(flask_app):
    """
    Populates the database with some test pulses. This will
    be torn down after every test.
    :param flask,Flask flask_app: the current flask app.
    :return:
    """
    with flask_app.app_context():
        db.drop_all()
        db.create_all()

        for i in range(10):
            p = Pulse(name='Test pulse {}'.format(i),
                      type='Primitive',
                      max_rabi_rate=10 * i,
                      polar_angle=0.1 * i)
            db.session.add(p)
        db.session.commit()
