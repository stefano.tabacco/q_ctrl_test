"""
Tests csv upload/download from the DB
"""
import pytest
import pkg_resources
import io

from flask_rest_jsonapi import JsonApiException

HEADERS = {
    'accept': "application/vnd.api+json",
    'content-type': "application/vnd.api+json",
}

from pulse.ext.csvfile import CSVPulsePublisher


def test_can_save_db_from_CSV_file(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    test_csv_file = pkg_resources.resource_filename(__name__, 'test_data/testdata.csv')
    pulses = CSVPulsePublisher(setup_app_for_testing).CSV_to_pulses(test_csv_file)
    assert 5 == len(pulses)


def test_cannot_accept_malformed_CSV_file(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    test_csv_file = pkg_resources.resource_filename(__name__, 'test_data/testdata_malformed.csv')
    with pytest.raises(JsonApiException) as excinfo:
        pulses = CSVPulsePublisher(setup_app_for_testing).CSV_to_pulses(test_csv_file)


def test_can_dump_db_into_CSV(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    data = CSVPulsePublisher(setup_app_for_testing).pulses_to_CSV()
    split_data = data.split('\n')
    assert len(split_data) == 12
    for i in range(1, 11):
        assert split_data[i].startswith('Test pulse')


def test_download_CSV_from_endpoint(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    response = flask_app.get('/csv/download', headers=HEADERS)
    assert '200 OK' == response.status


def test_upload_CSV_file_from_endpoint(setup_app_for_testing):
    flask_app = setup_app_for_testing.test_client()
    test_csv_file = pkg_resources.resource_filename(__name__, 'test_data/testdata.csv')
    with open(test_csv_file, 'rb') as f:
        stream = io.BytesIO(f.read())
        response = flask_app.post('/csv/upload', buffered=True,
                                  content_type='multipart/form-data',
                                  data={'file': (stream, 'test_upload.csv')},
                                  headers=HEADERS)
        assert '200 OK' == response.status
