from pulse.app import create_app
from pulse.model import db
from flask import render_template
import os

app = create_app(configClass=os.getenv('APP_SETTINGS', 'config.DevelopmentConfig'))


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    with app.app_context():
        db.create_all()

    # Start the application
    app.run(debug=True)
